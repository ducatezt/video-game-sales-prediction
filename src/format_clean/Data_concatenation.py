# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 14:36:09 2021

@author: theod

This code has been used to concatenate all the files we got from the scraping of the two datasets.
We had two datasets of 18 files of 1000 rows each. We had to concatenate them to have two main files.
"""

import pandas as pd

# For iterating over the data files
from os import listdir

# The directory where all the 
DIR = "../../data"

def concatenate_files(dir):
    """Concatenates the files of the given directory into two dataframes given their names

    Args:
        dir: The directory path

    Returns:
        (pandas.Dataframe): Two dataframes. One for the first dataset (only vgchartz data) and one for the second
        dataset (vgchartz and metacritics data)

    """
    # The final dataframes
    vgsales_df = None
    vgsales_metacritic_df = None

    # The count of files 
    vgsales_count = 0
    vgsales_metacritic_count = 0

    for data_file in listdir(dir):
        if data_file.endswith(".csv"):
            # 2nd dataset
            if not data_file.startswith("vgsales_metacritic"):
                if vgsales_count == 0:
                    # Means its the first one
                    vgsales_df = pd.read_csv(f"{dir}/{data_file}")
                else:
                    vgsales_df = vgsales_df.append(pd.read_csv(f"{dir}/{data_file}"))
                print(f"{data_file} : added : new df length {str(len(vgsales_df.index))}")
                vgsales_count += 1
                
            # 1st dataset
            else:
                if vgsales_metacritic_count == 0:
                    # Means its the first one
                    vgsales_metacritic_df = pd.read_csv(f"{dir}/{data_file}")
                else:
                    vgsales_metacritic_df = vgsales_metacritic_df.append(pd.read_csv(f"{dir}/{data_file}"))
                
                print(f"{data_file} : added : new df length {str(len(vgsales_metacritic_df.index))}")
                vgsales_metacritic_count += 1

    # Order by rank
    vgsales_df = vgsales_df.sort_values(by=['Rank'])
    vgsales_metacritic_df = vgsales_metacritic_df.sort_values(by=['Rank'])

    return vgsales_df, vgsales_metacritic_df

if __name__ == "__main__":
    
    print(f"Starting concatenation of files from: {DIR} directory")
    
    # Getting the two resulting dataframes
    vgsales_df, vgsales_metacritic_df = concatenate_files(DIR)
    
    # Testing if everything is ok
    print(vgsales_df.head())
    print(vgsales_metacritic_df.head())

    # Saving data
    vgsales_df.to_csv(f"{DIR}/vgsales_total.csv", sep=",", encoding='utf-8', index=False)
    vgsales_metacritic_df.to_csv(f"{DIR}/vgsales_metacritic_total.csv", sep=",", encoding='utf-8', index=False)
