# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 22:25:37 2021

@author: theod

This code scraps the VGChartz website in order to get games informations and sales
"""


####################################
###########   IMPORTS   ############
####################################


# Imports for BeautifulSoup
from bs4 import BeautifulSoup, element

# Imports for data handling
import pandas as pd
import numpy as np

# Imports for network connection
import urllib

# Other imports
import time


####################################
#######  GLOBAL VARIABLES   #######
####################################


VGCHARTZ_MAX_PAGES = 19     # Maximum number of pages we need to visit
vgchartz_page = 1          # Number of vgchartz pages already visited

games_scraped = 0           # The number of games scraped
current_game_index = 0      # The current game index

CSV_SAVE_PATH = "C:/Users/theod/Desktop/"

start_time = time.time()
finished = False

# VGChartz data lists
vgchartz_rank = []
vgchartz_name = []
vgchartz_platform = []
vgchartz_year = []
vgchartz_genre = []
vgchartz_critic_score = []
vgchartz_user_score = []
vgchartz_publisher = []
vgchartz_developer = []
vgchartz_sales_na = []
vgchartz_sales_pal = []
vgchartz_sales_jp = []
vgchartz_sales_other = []
vgchartz_sales_gl = []
vgchartz_url_name = []

# URL for the VGChartz website
VGCHARTZ_URL_START = 'https://www.vgchartz.com/games/games.php?page='
VGCHARTZ_URL_END = '&order=TotalSales&ownership=Both&showtotalsales=1&shownasales=1' \
    '&showpalsales=1&showjapansales=1&showothersales=1&showpublisher=1&showdeveloper=1' \
    '&showreleasedate=1&showlastupdate=0&showvgchartzscore=1&showcriticscore=1&showuserscore=1' \
    '&showshipped=0&ownership=Both&showvgchartzscore=0&results=1000'
  

####################################
############   MAIN   ##############
####################################


if __name__ == "__main__":

    while vgchartz_page <= VGCHARTZ_MAX_PAGES and not finished:

        # Connect to the VGChartz table. There are 1000 results per page.
        print("\n---------------------------------------------------\n")
        print(f"Connecting to VGChartz Page {str(vgchartz_page)}...")
        vgchartz_url = VGCHARTZ_URL_START + str(vgchartz_page) + VGCHARTZ_URL_END
        vgchartz_conn = urllib.request.urlopen(vgchartz_url)
        vgchartz_html = vgchartz_conn.read()
        print("connected.\n")

        parser = BeautifulSoup(vgchartz_html, "html.parser")
        
        # Getting the <a> tags containing the games pages URL (we'll go to the table rows from there)
        game_tags = list(filter(
            lambda x: x['href'].startswith('https://www.vgchartz.com/game/') if x.has_attr('href') else False,
            parser.find_all("a")
        ))
        
        # Parsing each table rows from this tag
        for tag in game_tags:
            
            # get different attributes traverse up the DOM tree (we go up to the corresponding game row)
            data = tag.parent.parent.find_all("td")
            
            # Get global sales first 
            # As we want to stop when there are no more sales values (0.0m or N/A)
            game_global_sales = float(data[8].get_text()[:-1]) if not data[8].string.startswith("N/A") else np.nan
            if game_global_sales == 0.0 or game_global_sales == np.nan:
                finished = True
                break
            vgchartz_sales_gl.append(game_global_sales)
            
            # add game name name to list
            vgchartz_name.append(" ".join(tag.string.split()))
            print(f"{games_scraped + 1} Fetch data for game {vgchartz_name[-1]}")

            # Get game number
            vgchartz_rank.append(np.int32(data[0].get_text()))

            # Get platform from alt of the img
            vgchartz_platform.append(data[3].find('img')['alt'])

            # Get publisher
            vgchartz_publisher.append(data[4].get_text())
            
            # Get developer
            vgchartz_developer.append(data[5].get_text())

            # From there, it is possible that the data is missing, so we handle them with np.nan
            # Get critic score
            vgchartz_critic_score.append(
                float(data[6].get_text()) if
                not data[6].string.startswith("N/A") else np.nan)

            # Get user score
            vgchartz_user_score.append(
                float(data[7].get_text()) if
                not data[7].string.startswith("N/A") else np.nan)

            # Get na sales
            vgchartz_sales_na.append(
                float(data[9].get_text()[:-1]) if
                not data[9].string.startswith("N/A") else np.nan)

            # Get pal sales
            vgchartz_sales_pal.append(
                float(data[10].get_text()[:-1]) if
                not data[10].string.startswith("N/A") else np.nan)

            # Get jp sales
            vgchartz_sales_jp.append(
                float(data[11].get_text()[:-1]) if
                not data[11].string.startswith("N/A") else np.nan)

            # Get other sales
            vgchartz_sales_other.append(
                float(data[12].get_text()[:-1]) if
                not data[12].string.startswith("N/A") else np.nan)

            # Get release year
            release_year = data[13].get_text().split()[-1]
            # reformat the year if not N/A (XX --> XXXX)
            if release_year.startswith('N/A'):
                vgchartz_year.append('N/A')
            else:
                if int(release_year) >= 80:
                    year_to_add = np.int32("19" + release_year)
                else:
                    year_to_add = np.int32("20" + release_year)
                vgchartz_year.append(year_to_add)
            
            
            game_page_url = tag['href']
            
            # Get url name (for metacritic scraping later on)
            vgchartz_url_name.append(game_page_url.rsplit('/', 2)[1])
            
            # Get the game genre
            # For that we need to visit the game page and scrap it too
            print("    - Scraping genre from game page")
            game_page_conn = urllib.request.urlopen(game_page_url)
            game_page_html = game_page_conn.read()
            sub_parser = BeautifulSoup(game_page_html, "html.parser")
            # Info boxes are inconsistents so we 
            # have to find all the h2 and traverse from that to the genre name
            h2_titles = sub_parser.find("div", {"id": "gameGenInfoBox"}).find_all('h2')
            # make a temporary tag here to search for the one that contains
            # the word "Genre"
            temp_tag = element.Tag
            for title in h2_titles:
                if title.string == 'Genre':
                    temp_tag = title
            vgchartz_genre.append(temp_tag.next_sibling.string)
            
            games_scraped += 1

        # Saving every 1000 games in order to have backup in case of crash or anything else
        # The CGChartz server is strange and makes the program crash around every 2000 games
        # So we have to split the data scraped into multiple sessions
        columns = {
            'Rank': vgchartz_rank,
            'Name': vgchartz_name,
            'Platform': vgchartz_platform,
            'Year': vgchartz_year,
            'Genre': vgchartz_genre,
            'Critic_Score': vgchartz_critic_score,
            'User_Score': vgchartz_user_score,
            'Publisher': vgchartz_publisher,
            'Developer': vgchartz_developer,
            'NA_Sales': vgchartz_sales_na,
            'PAL_Sales': vgchartz_sales_pal,
            'JP_Sales': vgchartz_sales_jp,
            'Other_Sales': vgchartz_sales_other,
            'Global_Sales': vgchartz_sales_gl,
            'Url_name' : vgchartz_url_name,
        }
        
        print("\n---------------------------------------------------\n")
        print(f"Time taken to scrap: {time.time() - start_time}")
        print(f"Number of VGChartz pages scraped: {vgchartz_page}")
        print(f"Total games scraped: {games_scraped}")
        
        print("\n---------------------------------------------------\n")
        df = pd.DataFrame(columns)
        print(f"Data scraped: {df.columns}")
        
        df = df[['Rank', 'Name', 'Platform', 'Year', 'Genre',
            'Publisher', 'Developer', 'Critic_Score', 'User_Score',
            'NA_Sales', 'PAL_Sales', 'JP_Sales', 'Other_Sales', 'Global_Sales',
            'Url_name']]
        
        print("\n---------------------------------------------------\n")
        print(f"Saving data to csv at: {CSV_SAVE_PATH}vgsales_{vgchartz_page}.csv")
        df.to_csv(f"{CSV_SAVE_PATH}vgsales_{vgchartz_page}.csv", sep=",", encoding='utf-8', index=False)
        
        # Reseting
        vgchartz_rank = []
        vgchartz_name = []
        vgchartz_platform = []
        vgchartz_year = []
        vgchartz_genre = []
        vgchartz_critic_score = []
        vgchartz_user_score = []
        vgchartz_publisher = []
        vgchartz_developer = []
        vgchartz_sales_na = []
        vgchartz_sales_pal = []
        vgchartz_sales_jp = []
        vgchartz_sales_other = []
        vgchartz_sales_gl = []
        vgchartz_url_name = []
        
        vgchartz_page += 1