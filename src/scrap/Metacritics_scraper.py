# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 17:31:39 2021

@author: theod

This code scraps the metacritic video games page and get the needed informations.
The video games come from the VGChartz dataset 1 and this code loops over each games
in this dataset to get the corresponding data from the other website
"""


####################################
###########   IMPORTS   ############
####################################


# Imports for BeautifulSoup
from bs4 import BeautifulSoup
import json

# Imports for data handling
import pandas as pd
import numpy as np

# Imports for network connection
import urllib

# Other imports
import time


####################################
#######  GLOBAL VARIABLES   #######
####################################


games_scraped = 0           # The number of games scraped
current_game_index = 0      # The current game index

CSV_INPUT_FILE_PATH = "C:/Users/theod/Desktop/vgsales_18000-19000.csv"
CSV_SAVE_PATH = "C:/Users/theod/Desktop/"

# Timing variables
start_time = time.time()
finished = False

# Metacritic need a specific type of genre in the URL
# that is different in VGchartz website so we have to make
# A correspondance table (VGchartz --> Metacritics)
vgchartz_platform_to_metacritic = {'PS3': 'playstation-3',
					   'X360': 'xbox-360',
					   'PC': 'pc',
					   'WiiU': 'wii-u',
					   '3DS': '3ds',
					   'PSV': 'playstation-vita',
					   'iOS': 'ios',
					   'Wii': 'wii',
					   'DS': 'ds',
					   'PSP': 'psp',
					   'PS2': 'playstation-2',
					   'PS': 'playstation',
					   'XB': 'xbox',
					   'GC': 'gamecube',
					   'GBA': 'game-boy-advance',
					   'DC': 'dreamcast',
					   'PS4': 'playstation-4',
					   'XOne': 'xbox-one'
					   }


####################################
##########   FUNCTIONS   ###########
####################################

def create_metacritic_url(vgchartz_game_platform, vgchartz_url_name):
	"""Creates a metacritics URL to the game page from the platform and the name of the game

	Args:
		vgchartz_game_platform: The platform of the game in "VGChartz" format
		vgchartz_url_name: The VGChartz game name (from its URL)

	Returns:
		(str): The Metacritics URL to the game page.

	"""
	url = None
	if vgchartz_game_platform in vgchartz_platform_to_metacritic:
		url = "https://www.metacritic.com/game/"
		url = url + vgchartz_platform_to_metacritic[vgchartz_game_platform] 
		url = url + "/" + vgchartz_url_name
	
	return url

def scrap(row):
	"""Scrap the metacritic game page of a game from its row in the VGChartz dataframe
	It uses its URL name and VGChartz platform to do so.

	Args:
		row: A Pandas Dataframe row with at least 'Platform' and 'Url_name' columns

	Returns:
		(Pandas Dataframe row): The new row with the metacritic data columns added

	"""

	global games_scraped
	global current_game_index
	
	metacritic_url = create_metacritic_url(row['Platform'], row['Url_name'])

	if metacritic_url:
        
        # Connect to the metacritic game page
		print("\n---------------------------------------------------\n")
		print(f"{games_scraped + 1} Connecting to page {str(metacritic_url)}")
		req = urllib.request.Request(metacritic_url)
		req.add_unredirected_header('User-Agent','Mozilla/5.0')
		try:
			metacritic_game_conn = urllib.request.urlopen(req, timeout = 4)
			metacritic_game_html = metacritic_game_conn.read()
			print("connected.\n")

			parser = BeautifulSoup(metacritic_game_html, "html.parser")
		except:
			row['Critic_score'] = np.nan
			row['Critic_count'] = np.nan
			row['User_score'] = np.nan
			row['User_count'] = np.nan
			row['Genres'] = np.nan
			row['Rating'] = np.nan
			return row
			
		# Get critic information
		try:
			res = parser.find("script",type="application/ld+json")
			js = json.loads(res.string)
			row['Critic_score'] = js['aggregateRating']['ratingValue']
			row['Critic_count'] = js['aggregateRating']['ratingCount']
		except:
			print("WARNING: Problem getting critic score information")
			row['Critic_score'] = np.nan
			row['Critic_count'] = np.nan
			pass
			
		# Get user information
		try:
			users = parser.find("div", class_="details side_details")
			row['User_score'] = users.find("div", class_="metascore_w").text.strip()
			raw_users_count = users.find("span", class_="count").a.text
			user_count = ''
			for c in raw_users_count:
				if c.isdigit(): user_count += c
			row['User_count'] = user_count.strip()
		except:
			print("WARNING: Problem getting user score information")
			row['User_score'] = np.nan
			row['User_count'] = np.nan
			pass
				
		# Get remaining information
		try:
			product_info = parser.find("div", class_="section product_details").find("div", class_="details side_details")
			genres = []
			for span in product_info.find("li", class_="summary_detail product_genre").find_all("span", class_="data"):
				genres.append(span.text.strip())
			row['Genres'] = genres
			row['Rating'] = product_info.find("li", class_="summary_detail product_rating").find("span", class_="data").text.strip()
		except:
			print("WARNING: Problem getting miscellaneous game information")
			row['Genres'] = np.nan
			row['Rating'] = np.nan
			pass

	else:
		row['Critic_score'] = np.nan
		row['Critic_count'] = np.nan
		row['User_score'] = np.nan
		row['User_count'] = np.nan
		row['Genres'] = np.nan
		row['Rating'] = np.nan

	games_scraped += 1
	current_game_index += 1

	return row


####################################
############   MAIN   ##############
####################################


if __name__ == "__main__":
    
	# Read the first dataset (vgchartz data only)
	df = pd.read_csv(CSV_INPUT_FILE_PATH)

	# Transforms the DF by scraping the metacritics website
	metacritics_data = df.apply(lambda row: scrap(row), axis=1)

	print("\n---------------------------------------------------\n")
	print(f"Time taken to scrap: {time.time() - start_time}")
	print(f"Total games scraped: {games_scraped}")

	print("\n---------------------------------------------------\n")
	print(f"Data scraped: {metacritics_data.columns}")

	print("\n---------------------------------------------------\n")
	print(f"Saving data to csv at: {CSV_SAVE_PATH}vgsales_metacritic_18000-19000.csv")
	metacritics_data.to_csv(f"{CSV_SAVE_PATH}vgsales_metacritic_18000-19000.csv", sep=",", encoding='utf-8', index=False)